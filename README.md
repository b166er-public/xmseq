# xmseq

e(X)treme (M)IDI (Seq)uencer


## Time cursor syntax

microsecond absolut - ```100```

microsecond relative - ```+100```

microsecond relative + shift - ```+100]```

beats absolut - ```1.5qn```

ticks (pulses) absolut - ```144p```

## TODO
- note-on-off event
