package dm.audio.midi.xmseq;

public class VelocityMapping {
    public static final short VELOCITY_MIN_VALUE = 0;
    public static final short VELOCITY_MAX_VALUE = 127;

    private final short velocityMinValue;
    private final short velocityMaxValue;
    private final Pattern pattern;

    public VelocityMapping(Pattern pattern) {
        this.velocityMinValue = VELOCITY_MIN_VALUE;
        this.velocityMaxValue = VELOCITY_MAX_VALUE;
        this.pattern = pattern;
    }
    public VelocityMapping(short velocityMinValue, short velocityMaxValue, Pattern pattern) {
        this.velocityMinValue = velocityMinValue;
        this.velocityMaxValue = velocityMaxValue;
        this.pattern = pattern;
    }
    public boolean isResponsible(short velocity) {
        return (velocity >= velocityMinValue) && (velocity <= velocityMaxValue);
    }
    public Pattern getPattern() {
        return pattern;
    }
}
