package dm.audio.midi.xmseq;

import java.util.Optional;

public class GateStateMachine {
    private final KeyBindingMode mode;
    private GateState state = GateState.CLOSED;

    public GateStateMachine(KeyBindingMode mode) {
        this.mode = mode;
    }

    public Optional<GateStateMachineEvent> processKeyEvent(KeyStateMachineEvent event) {

        Optional<GateStateMachineEvent> out = Optional.empty();

        switch(mode) {

            case HOLD -> {
                switch(state) {
                    case OPEN -> {
                        switch (event) {
                            case KEY_PRESSED -> {}
                            case KEY_RELEASED -> {state = GateState.CLOSED; out = Optional.of(GateStateMachineEvent.GATE_CLOSED); }
                        }
                    }
                    case CLOSED -> {
                        switch (event) {
                            case KEY_PRESSED -> {state = GateState.OPEN; out = Optional.of(GateStateMachineEvent.GATE_OPEND); }
                            case KEY_RELEASED -> {}
                        }
                    }
                }
            }
            case TOGGLE -> {
                switch(state) {
                    case OPEN -> {
                        switch (event) {
                            case KEY_PRESSED -> {state = GateState.CLOSED; out = Optional.of(GateStateMachineEvent.GATE_CLOSED); }
                            case KEY_RELEASED -> {}
                        }
                    }
                    case CLOSED -> {
                        switch (event) {
                            case KEY_PRESSED -> {state = GateState.OPEN; out = Optional.of(GateStateMachineEvent.GATE_OPEND); }
                            case KEY_RELEASED -> {}
                        }
                    }
                }
            }
        }

        return out;
    }
}
