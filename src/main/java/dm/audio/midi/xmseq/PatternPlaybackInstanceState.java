package dm.audio.midi.xmseq;

public enum PatternPlaybackInstanceState {
    QUANTIZATION_PAUSE,
    ATTACK,
    SUSTAIN,
    RELEASE,
    STOPPED
}
