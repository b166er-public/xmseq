package dm.audio.midi.xmseq;

import java.util.ArrayList;
import java.util.List;

import static dm.audio.midi.xmseq.PatternPlaybackInstanceState.*;

public class PatternPlaybackInstance {
    private PatternPlaybackInstanceState state = QUANTIZATION_PAUSE;
    private final Pattern pattern;
    private BusEventSequencePlaybackInstance currentSequencePlaybackInstance;
    private boolean stopRequested = false;

    public PatternPlaybackInstance(Pattern pattern, long startTimeStamp, long quantization) {
        this.pattern = pattern;

        // Calculate quantization pause
        if(quantization <= 0) {
            this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                BusEventSequence.EMPTY_EVENT_LIST,
                startTimeStamp
            );
        } else {
            long pause = quantization - (startTimeStamp % quantization);
            this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                new BusEventSequence(pause),
                startTimeStamp
            );
        }
    }

    public List<BusEvent> play(long currentTimeStamp) {
        List<BusEvent> out = new ArrayList<>();

        // Add all events which should have been played
        out.addAll(
            this.currentSequencePlaybackInstance.play(currentTimeStamp)
        );

        if(state == QUANTIZATION_PAUSE) {
            if(this.currentSequencePlaybackInstance.isPlaying()) {
                // Do nothing as we do not interrupt an event sequence
            } else {
                // We move towards the attack phase
                this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                    this.pattern.getAttack(),
                    this.currentSequencePlaybackInstance.getLastTimeStamp()
                );
                state = ATTACK;
            }
        }

        if(state == ATTACK) {
            if(this.currentSequencePlaybackInstance.isPlaying()) {
                // Do nothing as we do not interrupt an event sequence
            } else if(this.isStopRequested()) {
                // We move towards the release phase
                this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                    this.pattern.getRelease(),
                    this.currentSequencePlaybackInstance.getLastTimeStamp()
                );
                state = RELEASE;
            } else {
                boolean isSustainPhaseDefined = (this.pattern.getSustain().getDuration() > 0);
                if(isSustainPhaseDefined) {
                    // We move towards the sustain phase
                    this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                        this.pattern.getSustain(),
                        this.currentSequencePlaybackInstance.getLastTimeStamp()
                    );
                    state = SUSTAIN;
                } else {
                    // We move towards the release phase
                    this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                        this.pattern.getRelease(),
                        this.currentSequencePlaybackInstance.getLastTimeStamp()
                    );
                    state = RELEASE;
                }
            }
        }

        if(state == SUSTAIN) {
            if(this.currentSequencePlaybackInstance.isPlaying()) {
                // Do nothing as we do not interrupt an event sequence
            } else if(this.isStopRequested()) {
                // We move towards the release phase
                this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                    this.pattern.getRelease(),
                    this.currentSequencePlaybackInstance.getLastTimeStamp()
                );
                state = RELEASE;
            } else {
                // Retrigger the sustain sequence (infinite loop until termination)
                this.currentSequencePlaybackInstance = new BusEventSequencePlaybackInstance(
                    this.pattern.getSustain(),
                    this.currentSequencePlaybackInstance.getLastTimeStamp()
                );
                state = SUSTAIN;
            }
        }

        if(state == RELEASE) {
            if(this.currentSequencePlaybackInstance.isPlaying()) {
                // Do nothing as we do not interrupt an event sequence
            } else {
                state = STOPPED;
            }
        }

        if(state == STOPPED) {
            // Do nothing as this playback instance is already finalized
        }

        return out;
    }

    private boolean isStopRequested() {
        return this.stopRequested;
    }

    public void requestStop() {
        this.stopRequested = true;
    }

    public boolean isStopped() {
        return (this.state == STOPPED);
    }

    public PatternPlaybackInstanceState getState() {
        return this.state;
    }

}
