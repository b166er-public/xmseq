package dm.audio.midi.xmseq;

public enum KeyStateMachineEvent {
    KEY_PRESSED,
    KEY_RELEASED
}
