package dm.audio.midi.xmseq.runtime;

import dm.audio.midi.xmseq.*;
import dm.audio.midi.xmseq.configuration.*;
import dm.audio.midi.xmseq.messages.KeyMessageBase;
import dm.audio.midi.xmseq.messages.NoteOff;
import dm.audio.midi.xmseq.messages.NoteOn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class XmseqEngineConfigurationLoader {
    private static final Logger logger = LogManager.getLogger(XmseqEngineConfigurationLoader.class);
    private final XmseqEngine engine;
    private final XmseqConfiguration configuration;

    public XmseqEngineConfigurationLoader(XmseqEngine engine, XmseqConfiguration configuration) {
        this.engine = engine;
        this.configuration = configuration;
    }

    private KeyBinding mapKeyBinding(XmseqConfigurationKeyBinding configuration) {
        Key key;
        KeyBindingMode mode;
        long quantizationTime;
        List<VelocityMapping> velocityMappings = new ArrayList<>();

        key = new Key(
            (short) configuration.getMidiChannel(),
            (short) configuration.getMidiKey()
        );

        mode = KeyBindingMode.valueOf(configuration.getMode());

        quantizationTime = configuration.getQuantization();

        for(XmseqConfigurationVelocityMapping vm : configuration.getVelocityMappingList()) {
            velocityMappings.add(
                mapVelocityMapping(vm)
            );
        }

        return new KeyBinding(key, mode, quantizationTime, velocityMappings);
    }

    private VelocityMapping mapVelocityMapping(XmseqConfigurationVelocityMapping configuration) {
        short velocityMinValue;
        short velocityMaxValue;
        Pattern pattern;

        velocityMinValue = (short) configuration.getVelocityMin();
        velocityMaxValue = (short) configuration.getVelocityMax();
        pattern = mapPattern(configuration.getPattern());

        return new VelocityMapping(
            velocityMinValue,
            velocityMaxValue,
            pattern
        );
    }

    private Pattern mapPattern(XmseqConfigurationPattern configuration) {
        BusEventSequence attack;
        BusEventSequence sustain;
        BusEventSequence release;

        attack = mapBusEventSequence(configuration.getAttack());
        sustain = mapBusEventSequence(configuration.getSustain());
        release = mapBusEventSequence(configuration.getRelease());

        return new Pattern(
            attack,
            sustain,
            release
        );
    }

    private BusEventSequence mapBusEventSequence(XmseqConfigurationBusEventSequence configuration) {
        long duration = 0;
        List<BusEvent> events = new ArrayList<>();

        if(configuration != null) {
            duration = configuration.getDuration();

            for(XmseqConfigurationBusEvent be : configuration.getEventList()) {
                Optional<BusEvent> mappedEvent = mapBusEvent(be);
                mappedEvent.ifPresent(events::add);
            }
        } else {
            // An empty event sequence is created
        }


        return new BusEventSequence(
            duration,
            events
        );
    }

    private Optional<BusEvent> mapBusEvent(XmseqConfigurationBusEvent configuration) {
        String eventType = configuration.getType();
        if(NoteOn.XML_TYPE_NAME.equals(eventType)) {
            long timestamp = configuration.getTimeStamp();
            short midiChannel = Short.parseShort(
                    configuration.getArguments().getOrDefault(KeyMessageBase.XML_PARAMETER_CHANNEL, "0")
            );
            short midiKey = KeyParser.parse(
                    configuration.getArguments().getOrDefault(KeyMessageBase.XML_PARAMETER_KEY, "0")
            );
            short velocity = Short.parseShort(
                    configuration.getArguments().getOrDefault(NoteOn.XML_PARAMETER_VELOCITY,"0")
            );

            BusEvent out = new BusEvent(
                    timestamp,
                    new NoteOn(
                            midiChannel,
                            midiKey,
                            velocity
                    )
            );
            return Optional.of(out);
        } else if(NoteOff.XML_TYPE_NAME.equals(eventType)) {
            long timestamp = configuration.getTimeStamp();
            short midiChannel = Short.parseShort(
                configuration.getArguments().getOrDefault(KeyMessageBase.XML_PARAMETER_CHANNEL, "0")
            );
            short midiKey = KeyParser.parse(
                configuration.getArguments().getOrDefault(KeyMessageBase.XML_PARAMETER_KEY, "0")
            );
            short velocity = Short.parseShort(
                configuration.getArguments().getOrDefault(NoteOff.XML_PARAMETER_VELOCITY,"0")
            );

            BusEvent out = new BusEvent(
                timestamp,
                new NoteOff(
                    midiChannel,
                    midiKey,
                    velocity
                )
            );
            return Optional.of(out);
        } else {
            logger.warn("We currently not support bus events of type [ " + eventType + " ] !");
            return Optional.empty();
        }
    }
    public void load() {
        List<XmseqConfigurationKeyBinding> keyBindings = configuration.getKeyBindingList();
        for(XmseqConfigurationKeyBinding kb : keyBindings) {
            KeyController keyController = new KeyController(mapKeyBinding(kb));
            this.engine.addKeyController(keyController);
        }
    }
}
