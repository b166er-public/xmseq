package dm.audio.midi.xmseq.runtime;

import dm.audio.midi.xmseq.BusEvent;
import dm.audio.midi.xmseq.KeyController;
import dm.audio.midi.xmseq.configuration.XmseqConfiguration;
import dm.audio.midi.xmseq.utils.Time;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.requireNonNull;

public class XmseqEngine {

    private static final Logger logger = LogManager.getLogger(XmseqEngine.class);
    private static final String ENV_NAME_MINI_DEVICE_ID_IN = "XMSEQ_MIDI_IN";
    private static final String ENV_NAME_MINI_DEVICE_ID_OUT = "XMSEQ_MIDI_OUT";
    private final XmseqConfiguration configuration;
    private final String midiDeviceIdIn;
    private final String midiDeviceIdOut;
    private final Set<KeyController> keyControllerSet = new HashSet<>();

    private XmseqMidiInputStream midiInputStream = null;
    private XmseqMidiOutputStream midiOutputStream = null;

    private boolean isInitialized = false;

    private final AtomicBoolean terminationRequested = new AtomicBoolean(false);

    private final XmseqMasterClock masterClock;

    public XmseqEngine(XmseqConfiguration configuration) {
        requireNonNull(configuration);
        this.configuration = configuration;
        String ERROR_DEVICE_ID = "UNDEFINED";
        midiDeviceIdIn = System.getenv().getOrDefault(ENV_NAME_MINI_DEVICE_ID_IN, ERROR_DEVICE_ID);
        midiDeviceIdOut = System.getenv().getOrDefault(ENV_NAME_MINI_DEVICE_ID_OUT, ERROR_DEVICE_ID);

        if(midiDeviceIdIn.equals(ERROR_DEVICE_ID)) {
            logger.error("Please set " + ENV_NAME_MINI_DEVICE_ID_IN + " in your environment to set the MIDI IN device!");
        }

        if(midiDeviceIdOut.equals(ERROR_DEVICE_ID)) {
            logger.error("Please set " + ENV_NAME_MINI_DEVICE_ID_OUT + " in your environment to set the MIDI OUT device!");
        }

        this.masterClock = new XmseqMasterClock(
            this.configuration.getBpm()
        );
    }

    public void addKeyController(KeyController controller) {
        this.keyControllerSet.add(controller);
    }

    private void loadConfiguration() {
        XmseqEngineConfigurationLoader configLoader = new XmseqEngineConfigurationLoader(this, configuration);
        configLoader.load();
    }

    private void startMidiOutputStream() {
        Optional<XmseqMidiOutputStream> optMidiOutputStream = XmseqMidiOutputStream.getInstance(midiDeviceIdOut);
        if(optMidiOutputStream.isPresent()) {
            this.midiOutputStream = optMidiOutputStream.get();
        } else {
            logger.error("Could not open MIDI output stream!");
        }
    }

    private void startMidiInputStream() {
        Optional<XmseqMidiInputStream> optMidiInputStream = XmseqMidiInputStream.getInstance(midiDeviceIdIn);
        if(optMidiInputStream.isPresent()) {
            this.midiInputStream = optMidiInputStream.get();
        } else {
            logger.error("Could not open MIDI input stream!");
        }
    }


    public void init() {
        logger.info("Initialization of the XMSEQ engine started!");
        loadConfiguration();
        startMidiInputStream();
        startMidiOutputStream();

        // Check if initialization succeeded
        boolean isAnyKeyControllerDefined = !this.keyControllerSet.isEmpty();
        boolean isMidiInputOpend = (this.midiInputStream != null);
        boolean isMidiOutputOpend = (this.midiOutputStream != null);

        if(isAnyKeyControllerDefined && isMidiInputOpend && isMidiOutputOpend) {
            this.isInitialized = true;
            logger.info("Initialization of the XMSEQ engine completed!");
        } else {
            if(!isAnyKeyControllerDefined) { logger.error("No key controller defined!"); }
            if(!isMidiInputOpend) { logger.error("Could not open MIDI IN device!"); }
            if(!isMidiOutputOpend) { logger.error("Could not open MIDI OUT device!"); }
            this.isInitialized = false;
            logger.error("Failed to initialize the engine!");

            if(this.midiInputStream != null) {
                this.midiInputStream.close();
            }

            if(this.midiOutputStream != null) {
                this.midiOutputStream.close();
            }
        }
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    private void performCycle(long now) {

        // Read all MIDI messages available
        List<BusEvent> events = new ArrayList<>();
        boolean furtherMessagesAvailable = true;
        while(furtherMessagesAvailable) {
            Optional<BusEvent> nextBusEvent = this.midiInputStream.read(now);
            if(nextBusEvent.isPresent()) {
                events.add(nextBusEvent.get());
            } else {
                furtherMessagesAvailable = false;
            }
        }

        // Send all received messages to all key controller
        for(BusEvent be : events) {
            for(KeyController kc : this.keyControllerSet) {
                kc.processBusEvent(be, now);
            }
        }

        // Process all playback instance states of key controller
        for(KeyController kc : this.keyControllerSet) {
            kc.processPlaybackInstanceState();
        }

        // Collect all messages to send out
        List<BusEvent> eventsOut = new ArrayList<>();
        for(KeyController kc : this.keyControllerSet) {
            eventsOut.addAll(
                kc.play(now)
            );
        }

        // Evaluate master clock
        Optional<BusEvent> masterClockPulse = this.masterClock.play(now);
        if(masterClockPulse.isPresent()) {
            eventsOut.add(masterClockPulse.get());
        }

        // Send out all generated messages
        for(BusEvent be : eventsOut) {
            this.midiOutputStream.write(be.getMessage());
        }
    }

    public void requestTermination() {
        this.terminationRequested.set(true);
    }

    public void run() {
        if(!isInitialized) {
            logger.error("Can not start the engine as it is not initialized yet!");
        } else {
            logger.info("XMSEQ Engine running ...");

            this.midiOutputStream.write(
                masterClock.start()
            );

            while(!terminationRequested.get()) {
                this.performCycle(
                    Time.nowInUs()
                );
            }

            this.midiOutputStream.write(
                masterClock.stop()
            );

            this.midiOutputStream.close();
            this.midiInputStream.close();
            logger.info("XMSEQ Engine stopped!");
        }
    }

}
