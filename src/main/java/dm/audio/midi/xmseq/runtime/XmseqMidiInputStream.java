package dm.audio.midi.xmseq.runtime;

import dm.audio.midi.xmseq.BusEvent;
import dm.audio.midi.xmseq.messages.NoOperation;
import dm.audio.midi.xmseq.messages.NoteOff;
import dm.audio.midi.xmseq.messages.NoteOn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sound.midi.*;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class XmseqMidiInputStream implements Receiver{
    private static final Logger logger = LogManager.getLogger(XmseqMidiInputStream.class);

    private final MidiDevice midiDevice;

    private final Queue<MidiMessage> messageQueue = new ConcurrentLinkedQueue<>();
    public XmseqMidiInputStream(MidiDevice deviceHandler) {
        this.midiDevice = deviceHandler;
        try {
            this.midiDevice.getTransmitter().setReceiver(this);
        } catch(Exception e) {
            logger.error("Could do not setup received. Reason : " + e.getMessage());
        }

    }

    public Optional<BusEvent> read(long timestamp) {
        if(messageQueue.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(
                mapMessage(
                    timestamp,
                    messageQueue.poll()
                )
            );
        }

    }

    private BusEvent mapMessage(long timestamp, MidiMessage msg) {
        if (msg instanceof ShortMessage msgTyped) {
            short midiChannel = (short) msgTyped.getChannel();
            int midiStatus = msgTyped.getStatus();
            short midiKey = (short) msgTyped.getData1();
            short midiVelocity = (short) msgTyped.getData2();

            switch(midiStatus) {
                case ShortMessage.NOTE_ON -> {
                    // I like MIDI jokes :)
                    if(midiVelocity > 0) {
                        return new BusEvent(
                            timestamp,
                            new NoteOn(midiChannel, midiKey, midiVelocity)
                        );
                    } else {
                        return new BusEvent(
                            timestamp,
                            new NoteOff(midiChannel, midiKey, midiVelocity)
                        );
                    }

                }
                case ShortMessage.NOTE_OFF -> {
                    return new BusEvent(
                        timestamp,
                        new NoteOff(midiChannel, midiKey, midiVelocity)
                    );
                }

                case ShortMessage.TIMING_CLOCK, ShortMessage.ACTIVE_SENSING -> {
                    // We ignore the TIMING CLOCK as we are master here !!!
                    // We ignore the ACTIVE SENSING as we are master here !!!
                    return new BusEvent(
                        timestamp,
                        new NoOperation()
                    );
                }

                // We ignore ACTIVE SENSING as we simply do not care :)

                default -> {
                    logger.warn("Currently command of type [ " + midiStatus + " ] is not supported!");
                    return new BusEvent(
                        timestamp,
                        new NoOperation()
                    );
                }
            }
        } else {
            logger.warn("Currently midi messages of type [ " + msg.getClass().getCanonicalName() + " ] is not supported!");
            return new BusEvent(
                timestamp,
                new NoOperation()
            );
        }
    }

    public static Optional<XmseqMidiInputStream> getInstance(String deviceId) {
        try {
            MidiDevice.Info deviceInfo = XmseqMidiDeviceSupport.getDeviceById(deviceId);
            MidiDevice deviceHandler = MidiSystem.getMidiDevice(deviceInfo);
            deviceHandler.open();

            if(!deviceHandler.isOpen()) {
                logger.error("Could not open MIDI device!");
                return Optional.empty();
            }

            return Optional.of(
                new XmseqMidiInputStream(deviceHandler)
            );
        } catch(Exception e) {
            logger.error("Could not open MIDI output stream. Reason : " + e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public void send(MidiMessage message, long timeStamp) {
        this.messageQueue.add(message);
    }

    @Override
    public void close() {
        this.midiDevice.close();
        logger.info("MIDI Input Stream closed!");
    }
}
