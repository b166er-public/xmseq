package dm.audio.midi.xmseq.runtime;

import org.apache.commons.codec.digest.DigestUtils;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class XmseqMidiDeviceSupport {

    private static String deriveIdFromName(String name)
    {
        return DigestUtils.md5Hex(name).toUpperCase().substring(0,4);
    }

    public static Map<String,MidiDevice.Info> getDeviceMap() throws MidiUnavailableException
    {
        Map<String,MidiDevice.Info> result = new HashMap<>();

        MidiDevice.Info[] deviceInfoList = MidiSystem.getMidiDeviceInfo();

        for(int n = 0; n < deviceInfoList.length; n++)
        {
            MidiDevice.Info info = deviceInfoList[n];

            MidiDevice device = MidiSystem.getMidiDevice(info);

            String name = Integer.toString(n) + " " + info.getName();
            String id = deriveIdFromName(name);
            result.put(id, info);
        }

        return result;

    }

    public static MidiDevice.Info getDeviceById(String id) throws MidiUnavailableException
    {
        requireNonNull(id);
        Map<String, MidiDevice.Info> deviceMap = getDeviceMap();
        MidiDevice.Info device = deviceMap.get(id);
        if(device != null)
        {
            return device;
        }
        else
        {
            throw new RuntimeException("No MIDI device with id [ " + id + " ] found!");
        }
    }
}

