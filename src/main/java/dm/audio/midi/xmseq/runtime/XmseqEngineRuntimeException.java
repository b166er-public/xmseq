package dm.audio.midi.xmseq.runtime;

public class XmseqEngineRuntimeException extends RuntimeException {
    public XmseqEngineRuntimeException() {
    }

    public XmseqEngineRuntimeException(String message) {
        super(message);
    }

    public XmseqEngineRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public XmseqEngineRuntimeException(Throwable cause) {
        super(cause);
    }

    public XmseqEngineRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
