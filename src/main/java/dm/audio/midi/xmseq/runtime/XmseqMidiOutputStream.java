package dm.audio.midi.xmseq.runtime;

import dm.audio.midi.xmseq.messages.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sound.midi.*;
import java.util.Optional;

public class XmseqMidiOutputStream {

    private static final Logger logger = LogManager.getLogger(XmseqMidiOutputStream.class);

    private final MidiDevice midiDevice;
    private final Receiver midiReceiver;

    public XmseqMidiOutputStream(MidiDevice midiDevice, Receiver midiReceiver) {
        this.midiDevice = midiDevice;
        this.midiReceiver = midiReceiver;
    }

    public void write(MessageBase message) {
        try {
            if(message instanceof NoteOff msgTyped) {
                ShortMessage midiMessage = new ShortMessage(
                    ShortMessage.NOTE_OFF,
                    msgTyped.key.midiChannel,
                    msgTyped.key.midiKey,
                    msgTyped.midiVelocity
                );
                midiReceiver.send(midiMessage, -1L);
            } else if(message instanceof NoteOn msgTyped) {
                ShortMessage midiMessage = new ShortMessage(
                    ShortMessage.NOTE_ON,
                    msgTyped.key.midiChannel,
                    msgTyped.key.midiKey,
                    msgTyped.midiVelocity
                );
                midiReceiver.send(midiMessage, -1L);
            } else if(message instanceof TimingClock) {
                midiReceiver.send(new ShortMessage(ShortMessage.TIMING_CLOCK),-1L);
            } else if(message instanceof NoOperation) {
                // Do nothing as it is a NOP command
            } else if(message instanceof PlaybackStart) {
                midiReceiver.send(new ShortMessage(ShortMessage.START),-1L);
            } else if(message instanceof PlaybackStop) {
                midiReceiver.send(new ShortMessage(ShortMessage.STOP),-1L);
            } else {
                logger.warn("Currently messages of type [ " + message.getClass().getCanonicalName() + " ] are not supported for MIDI OUT!");
            }
        } catch (Exception e) {
            String msg = "Could not write to MIDI OUT. Reason : " + e.getMessage();
            logger.error(msg);
            throw new XmseqEngineRuntimeException(msg, e);
        }
    }

    public void close() {
        this.midiReceiver.close();
        this.midiDevice.close();
        logger.info("MIDI output stream closed!");
    }

    public static Optional<XmseqMidiOutputStream> getInstance(String deviceId) {
        try {
            MidiDevice.Info deviceInfo = XmseqMidiDeviceSupport.getDeviceById(deviceId);
            MidiDevice deviceHandler = MidiSystem.getMidiDevice(deviceInfo);
            deviceHandler.open();

            if(!deviceHandler.isOpen()) {
                logger.error("Could not open MIDI device!");
                return Optional.empty();
            }

            Receiver midiReceiver = deviceHandler.getReceiver();

            return Optional.of(
                new XmseqMidiOutputStream(deviceHandler, midiReceiver)
            );
        } catch(Exception e) {
            logger.error("Could not open MIDI output stream. Reason : " + e.getMessage());
        }
        return Optional.empty();
    }
}
