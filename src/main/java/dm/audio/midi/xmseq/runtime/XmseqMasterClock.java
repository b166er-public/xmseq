package dm.audio.midi.xmseq.runtime;

import dm.audio.midi.xmseq.BusEvent;
import dm.audio.midi.xmseq.messages.MessageBase;
import dm.audio.midi.xmseq.messages.PlaybackStart;
import dm.audio.midi.xmseq.messages.PlaybackStop;
import dm.audio.midi.xmseq.messages.TimingClock;
import dm.audio.midi.xmseq.utils.Time;

import java.util.Optional;

public class XmseqMasterClock {
    private long lastPulseTimestampUs = 0;
    private final long bpm;
    private final long dt;

    public XmseqMasterClock(long bpm) {
        this.bpm = bpm;
        this.dt = ((long)(((double)Time.usPerBeat(this.bpm)) / 24.0));
    }

    public MessageBase start() {
        this.lastPulseTimestampUs = 0;
        return new PlaybackStart();
    }

    public Optional<BusEvent> play(long timestamp) {
        if(timestamp - lastPulseTimestampUs >= dt) {
            lastPulseTimestampUs = timestamp;
            return Optional.of(
                new BusEvent(
                    timestamp,
                    new TimingClock()
                )
            );
        } else {
            return Optional.empty();
        }
    }

    public MessageBase stop() {
        return new PlaybackStop();
    }
}
