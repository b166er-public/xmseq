package dm.audio.midi.xmseq;

public enum KeyState {
    PRESSED,
    RELEASED
}
