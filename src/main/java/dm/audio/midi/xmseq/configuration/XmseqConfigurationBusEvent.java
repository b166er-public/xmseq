package dm.audio.midi.xmseq.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.HashMap;
import java.util.Map;

public class XmseqConfigurationBusEvent {

    private static final Logger logger = LogManager.getLogger(XmseqConfigurationBusEvent.class);

    public final static String XML_TYPE_NAME = "event";

    public final static String XML_ATTRIBUTE_TIMESTAMP = "ts";
    public final static String XML_ATTRIBUTE_TYPE = "type";
    private final Long timeStamp;
    private final String type;
    private final Map<String, String> arguments;

    public XmseqConfigurationBusEvent(Long timeStamp, String type, Map<String, String> arguments) {
        this.timeStamp = timeStamp;
        this.type = type;
        this.arguments = arguments;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getArguments() {
        return arguments;
    }

    public static XmseqConfigurationBusEvent parse(long bpm, long tpb, TimeCursor tc, Element root) {

        // Extract fields
        String fieldTimestamp = "";
        String fieldType = "";
        Map<String, String> attributes = new HashMap<>();

        NamedNodeMap attributeList = root.getAttributes();
        int numOfAttributes = attributeList.getLength();
        for(int a=0; a < numOfAttributes; a++) {
            Node attribute = attributeList.item(a);
            String name = attribute.getNodeName();
            String value = attribute.getNodeValue();

            if(XML_ATTRIBUTE_TIMESTAMP.equals(name)) {
                fieldTimestamp = value;
            } else if(XML_ATTRIBUTE_TYPE.equals(name)) {
                fieldType = value;
            } else {
                attributes.put(name, value);
            }
        }

        // Evaluate time cursor
        long timestamp = tc.evaluate(bpm,tpb, fieldTimestamp);

        // Emit new event
        return new XmseqConfigurationBusEvent(timestamp, fieldType, attributes);
    }
}
