package dm.audio.midi.xmseq.configuration;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class XmseqConfigurationVelocityMapping {
    private static final Logger logger = LogManager.getLogger(XmseqConfigurationVelocityMapping.class);
    public static final String XML_TYPE_NAME = "velocity-mapping";

    public final static String XML_ATTRIBUTE_VELOCITY_MIN = "min";
    public final static String XML_ATTRIBUTE_VELOCITY_MAX = "max";

    private final int velocityMin;
    private final int velocityMax;
    private final XmseqConfigurationPattern pattern;

    public XmseqConfigurationVelocityMapping(int velocityMin, int velocityMax, XmseqConfigurationPattern pattern) {
        this.velocityMin = velocityMin;
        this.velocityMax = velocityMax;
        this.pattern = pattern;
    }

    public int getVelocityMin() {
        return velocityMin;
    }

    public int getVelocityMax() {
        return velocityMax;
    }

    public XmseqConfigurationPattern getPattern() {
        return pattern;
    }

    public static XmseqConfigurationVelocityMapping parse(
        XmseqConfiguration context,
        Element root
    )
    {
        String attrVelocityMin = root.getAttribute(XML_ATTRIBUTE_VELOCITY_MIN);
        String attrVelocityMax = root.getAttribute(XML_ATTRIBUTE_VELOCITY_MAX);

        int velocityMin;
        int velocityMax;

        if( (attrVelocityMin == null) || (attrVelocityMin.length() == 0) ) {
            velocityMin = 0;
        }
        else if(StringUtils.isNumeric(attrVelocityMin)) {
            velocityMin = Integer.parseInt(attrVelocityMin);
        } else {
            logger.error("Minimal velocity has to be an integer!");
            velocityMin = 0;
        }

        if( (attrVelocityMax == null) || (attrVelocityMax.length() == 0) ) {
            velocityMax = 0;
        }
        else if(StringUtils.isNumeric(attrVelocityMax)) {
            velocityMax = Integer.parseInt(attrVelocityMax);
        } else {
            logger.error("Maximum velocity has to be an integer!");
            velocityMax = 0;
        }

        XmseqConfigurationPattern pattern = null;

        NodeList childNodes = root.getChildNodes();
        int numberOfChildren = childNodes.getLength();
        for(int c = 0; c < numberOfChildren; c++) {
            Node child = childNodes.item(c);
            boolean isElementNode = child.getNodeType() == Node.ELEMENT_NODE;
            if(isElementNode) {
                Element childElement = (Element) child;
                String tagName = childElement.getTagName();
                boolean isCorrectTagName = XmseqConfigurationPattern.XML_TYPE_NAME.equals(tagName);
                if(isCorrectTagName) {
                    pattern = XmseqConfigurationPattern.parse(
                        context,
                        (Element) child
                    );
                } else {
                    logger.warn("Velocity mapping expected a pattern element but received [ " + tagName + " ] !");
                }
            }
        }

        return new XmseqConfigurationVelocityMapping(
            velocityMin,
            velocityMax,
            pattern
        );
    }
}
