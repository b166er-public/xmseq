package dm.audio.midi.xmseq.configuration;

import dm.audio.midi.xmseq.Key;
import dm.audio.midi.xmseq.KeyBinding;
import dm.audio.midi.xmseq.KeyBindingMode;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class XmseqConfigurationKeyBinding {

    private static final Logger logger = LogManager.getLogger(XmseqConfigurationKeyBinding.class);

    public final static String XML_TYPE_NAME = "key-binding";

    public final static String XML_ATTRIBUTE_MIDI_CHANNEL = "channel";
    public final static String XML_ATTRIBUTE_MIDI_KEY = "key";
    public final static String XML_ATTRIBUTE_MIDI_QUANTIZATION = "quantization";
    public final static String XML_ATTRIBUTE_MODE = "mode";


    private final int midiChannel;
    private final int midiKey;
    private final String mode;
    private final long quantization;
    private final List<XmseqConfigurationVelocityMapping> velocityMappingList;

    public XmseqConfigurationKeyBinding(
        int midiChannel,
        int midiKey,
        String mode,
        long quantization,
        List<XmseqConfigurationVelocityMapping> velocityMappingList
    ) {
        this.midiChannel = midiChannel;
        this.midiKey = midiKey;
        this.mode = mode;
        this.quantization = quantization;
        this.velocityMappingList = velocityMappingList;
    }

    public int getMidiChannel() {
        return midiChannel;
    }

    public int getMidiKey() {
        return midiKey;
    }

    public long getQuantization() {
        return quantization;
    }

    public String getMode() {
        return mode;
    }

    public List<XmseqConfigurationVelocityMapping> getVelocityMappingList() {
        return velocityMappingList;
    }

    public static XmseqConfigurationKeyBinding parse(XmseqConfiguration context, Element root) {
        String attrMidiChannel = root.getAttribute(XML_ATTRIBUTE_MIDI_CHANNEL);
        String attrMidiKey = root.getAttribute(XML_ATTRIBUTE_MIDI_KEY);
        String attrMode = root.getAttribute(XML_ATTRIBUTE_MODE);
        String attrQuantization = root.getAttribute(XML_ATTRIBUTE_MIDI_QUANTIZATION);

        int midiChannel;
        int midiKey;
        String mode = null;
        long quantization;

        if(StringUtils.isNumeric(attrMidiChannel)) {
            midiChannel = Integer.parseInt(attrMidiChannel);
        } else {
            logger.error("MIDI Channel has to be an integer!");
            midiChannel = 0;
        }

        midiKey = KeyParser.parse(attrMidiKey);

        if(attrMode.length() > 0) {
            boolean isModeValid = false;
            for(KeyBindingMode kbm : KeyBindingMode.values()) {
                if(kbm.name().equals(attrMode)) {
                    isModeValid = true;
                    break;
                }
            }

            if(isModeValid) {
                mode = attrMode;
            } else {
                mode = "HOLD";
                logger.error("The specified key binding mode [ " + attrMode + " ] is invalid! Will use HOLD as default!");
            }
        }

        TimeParser tp = new TimeParser(context.getBpm(), context.getTpb());
        if(tp.valid(attrQuantization)) {
            quantization = tp.parse(attrQuantization);
        } else {
            logger.error("Launch quantization has to be an valid time expression!");
            quantization = 0L;
        }

        List<XmseqConfigurationVelocityMapping> velocityMappings = new ArrayList<>();

        NodeList childNodes = root.getChildNodes();
        int numberOfChildren = childNodes.getLength();
        for(int c = 0; c < numberOfChildren; c++) {
            Node child = childNodes.item(c);
            boolean isElementNode = child.getNodeType() == Node.ELEMENT_NODE;
            if(isElementNode) {
                Element childElement = (Element) child;
                String tagName = childElement.getTagName();
                boolean isVelocityMapping = XmseqConfigurationVelocityMapping.XML_TYPE_NAME.equals(tagName);
                boolean isPattern = XmseqConfigurationPattern.XML_TYPE_NAME.equals(tagName);
                if(isVelocityMapping) {
                    velocityMappings.add(
                        XmseqConfigurationVelocityMapping.parse(
                            context,
                            childElement
                        )
                    );
                } else if(isPattern) {
                    int velMin = 0;
                    int velMax = 127;
                    XmseqConfigurationPattern pattern = XmseqConfigurationPattern.parse(
                        context,
                        childElement
                    );
                    velocityMappings.add(
                        new XmseqConfigurationVelocityMapping(
                            velMin,
                            velMax,
                            pattern
                        )
                    );
                } else {
                    logger.info("The node type [ " + tagName + " ] is currently not supported!");
                }
            }
        }

        return new XmseqConfigurationKeyBinding(
            midiChannel,
            midiKey,
            mode,
            quantization,
            velocityMappings
        );

    }
}
