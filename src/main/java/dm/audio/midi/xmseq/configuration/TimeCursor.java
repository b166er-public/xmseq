package dm.audio.midi.xmseq.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeCursor {
    private static final Logger logger = LogManager.getLogger(TimeCursor.class);
    private final static Pattern regexPattern = Pattern.compile(
        "([\\+\\-]?)(?:((\\d+)|(\\d+\\.\\d+)))(qn|p?)(]?)$",
        Pattern.MULTILINE
    );
    private long position = 0L;

    public TimeCursor() {}

    public long getPosition() {
        return this.position;
    }

    public long evaluate(long bpm, long tpb, String fieldTimestamp) {
        Matcher matcher = regexPattern.matcher(fieldTimestamp);
        if(matcher.find()) {
            String grRelative = matcher.group(1);
            String grValue = matcher.group(2);
            String grUnit = matcher.group(5);
            String grShift = matcher.group(6);

            Double value = Double.parseDouble(grValue);

            double usPerUnit;
            if("qn".equals(grUnit)) {
                usPerUnit = (1000000.0 * 60) / ((double)bpm);
            } else if("p".equals(grUnit)) {
                double usPerBeat = (1000000.0 * 60) / ((double)bpm);
                usPerUnit = usPerBeat / ((double)tpb);
            } else {
                usPerUnit = 1.0;
            }

            long valueUs = (long)(value * usPerUnit);

            long result = this.position;
            if( (grRelative != null) && (grRelative.length()>0) ) {
                if("+".equals(grRelative)) {
                    result = this.position + valueUs;
                } else if("-".equals(grRelative)) {
                    result = this.position - valueUs;
                } else {
                    logger.error("The operator [ " + grRelative + " ] is not supported yet!");
                }
            } else {
                result = valueUs;
            }

            if( (grShift != null) && (grShift.length()>0) ) {
                this.position = result;
            }

            return result;

        } else {
            logger.error("The time expression [ " + fieldTimestamp + " ] is invalid!");
            return this.position;
        }
    }
}
