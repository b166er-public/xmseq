package dm.audio.midi.xmseq.configuration;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class XmseqConfigurationBusEventSequence {

    private static final Logger logger = LogManager.getLogger(XmseqConfigurationBusEventSequence.class);

    public final static String XML_TYPE_NAME = "sequence";

    public final static String XML_ATTRIBUTE_ID = "id";
    public final static String XML_ATTRIBUTE_REF = "ref";
    public final static String XML_ATTRIBUTE_IMPORT = "import";
    public final static String XML_ATTRIBUTE_DURATION = "duration";

    private final Long duration;
    private final List<XmseqConfigurationBusEvent> eventList;

    public XmseqConfigurationBusEventSequence(Long duration, List<XmseqConfigurationBusEvent> eventList) {
        this.duration = duration;
        this.eventList = eventList;
    }

    public Long getDuration() {
        return duration;
    }

    public List<XmseqConfigurationBusEvent> getEventList() {
        return new ArrayList<>(eventList);
    }

    public static XmseqConfigurationBusEventSequence parse(XmseqConfiguration context, Element root) {
        boolean isRef = root.hasAttribute(XML_ATTRIBUTE_REF);
        boolean isImport = root.hasAttribute(XML_ATTRIBUTE_IMPORT);

        XmseqConfigurationBusEventSequence out = null;

        if(isRef) {
            String refValue = root.getAttribute(XML_ATTRIBUTE_REF);
            if(context.isEventSequenceKnown(refValue)) {
                out = context.getEventSequence(refValue);
            } else {
                logger.error("The event sequence [ " + refValue + " ] is unknown!");
            }

        } else if(isImport) {
            logger.warn("Import of event sequences is not implemented yet!");
        } else {
            // ... read pattern duration
            Long patternDuration = -1L;
            if(root.hasAttribute(XML_ATTRIBUTE_DURATION)) {
                String durationValueString = root.getAttribute(XML_ATTRIBUTE_DURATION);

                TimeParser tp = new TimeParser(context.getBpm(), context.getTpb());

                if(tp.valid(durationValueString)) {
                    patternDuration = tp.parse(durationValueString);
                } else {
                    logger.error("Duration has to be a valid time expression!");
                }
            }

            // ... read bus events
            List<XmseqConfigurationBusEvent> events = new ArrayList<>();
            TimeCursor tc = new TimeCursor();
            NodeList childNodes = root.getChildNodes();
            int numOfChilds = childNodes.getLength();
            for(int c = 0; c < numOfChilds; c++) {
                Node child = childNodes.item(c);
                boolean isElementNode = child.getNodeType() == Node.ELEMENT_NODE;
                if(isElementNode) {
                    Element childElement = (Element) child;
                    boolean isCorrectTag = XmseqConfigurationBusEvent.XML_TYPE_NAME.equals(childElement.getTagName());
                    if(isCorrectTag) {
                        events.add(
                            XmseqConfigurationBusEvent.parse(
                                context.getBpm(),
                                context.getTpb(),
                                tc,
                                childElement
                            )
                        );
                    }
                }
            }

            out = new XmseqConfigurationBusEventSequence(patternDuration,events);
        }

        // Add to map iff has ID
        boolean hasId = root.hasAttribute(XML_ATTRIBUTE_ID);
        if(hasId) {
            context.putEventSequence(
                root.getAttribute(XML_ATTRIBUTE_ID),
                out
            );
        }

        return out;
    }
}
