package dm.audio.midi.xmseq.configuration;

public class XmseqConfigurationLoadException extends RuntimeException {
    public XmseqConfigurationLoadException() {
    }

    public XmseqConfigurationLoadException(String message) {
        super(message);
    }

    public XmseqConfigurationLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public XmseqConfigurationLoadException(Throwable cause) {
        super(cause);
    }

    public XmseqConfigurationLoadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
