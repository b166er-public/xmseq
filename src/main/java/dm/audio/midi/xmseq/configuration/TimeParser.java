package dm.audio.midi.xmseq.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeParser {

    private final long bpm;
    private final long tpb;

    public TimeParser(long bpm, long tpb) {
        this.bpm = bpm;
        this.tpb = tpb;
    }

    private static final Logger logger = LogManager.getLogger(TimeParser.class);
    private final static Pattern regexPattern = Pattern.compile(
            "(((\\d+)|(\\d+\\.\\d+)))(qn|p?)$",
            Pattern.MULTILINE
    );

    public boolean valid(String expression) {
        Matcher matcher = regexPattern.matcher(expression);
        return matcher.find();
    }

    public long parse(String expression) {
        Matcher matcher = regexPattern.matcher(expression);
        if(matcher.find()) {
            String grValue = matcher.group(1);
            String grUnit = matcher.group(5);

            Double value = Double.parseDouble(grValue);

            double usPerUnit;
            if("qn".equals(grUnit)) {
                usPerUnit = (1000000.0 * 60) / ((double)bpm);
            } else if("p".equals(grUnit)) {
                double usPerBeat = (1000000.0 * 60) / ((double)bpm);
                usPerUnit = usPerBeat / ((double)tpb);
            } else {
                usPerUnit = 1.0;
            }

            long valueUs = (long)(value * usPerUnit);

            return valueUs;

        } else {
            logger.error("The time expression [ " + expression + " ] is invalid!");
            return 0;
        }
    }
}
