package dm.audio.midi.xmseq.configuration;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KeyParser {

    private static final Logger logger = LogManager.getLogger(KeyParser.class);
    private final static Pattern regexPattern = Pattern.compile(
        "(C|C#|D|D#|E|F|F#|G|G#|A|A#|H)(-1|0|1|2|3|4|5|6|7|8|9)",
        Pattern.MULTILINE
    );

    public static boolean isValid(String expression) {
        return regexPattern.matcher(expression).find();
    }

    public static short parse(String expression) {
        if(StringUtils.isNumeric(expression)) {
            return Short.parseShort(expression);
        } else if(KeyParser.isValid(expression)) {
            return KeyParser.parseMusicalNotation(expression);
        } else {
            logger.error("MIDI Key has to be an integer or in musical notation!");
            return 0;
        }
    }

    private static short parseMusicalNotation(String expression) {
        Matcher matcher = regexPattern.matcher(expression);
        if(matcher.find()) {
            String grNote = matcher.group(1);
            String grOctave = matcher.group(2);

            short key = 0;

            if(grNote.equals("C")) {
                key = 0;
            } else if(grNote.equals("C#")) {
                key = 1;
            } else if(grNote.equals("D")) {
                key = 2;
            } else if(grNote.equals("D#")) {
                key = 3;
            } else if(grNote.equals("E")) {
                key = 4;
            } else if(grNote.equals("F")) {
                key = 5;
            } else if(grNote.equals("F#")) {
                key = 6;
            } else if(grNote.equals("G")) {
                key = 7;
            } else if(grNote.equals("G#")) {
                key = 8;
            } else if(grNote.equals("A")) {
                key = 9;
            } else if(grNote.equals("A#")) {
                key = 10;
            } else if(grNote.equals("B")) {
                key = 11;
            }

            key += (Short.parseShort(grOctave) + 1) * 12;

            return key;
        } else {
            logger.error("Could not parse the key [ " + expression + " ] !");
            return 0;
        }
    }
}
