package dm.audio.midi.xmseq.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.util.Objects.requireNonNull;

public class XmseqConfigurationParser {
    private static final Logger logger = LogManager.getLogger(XmseqConfigurationParser.class);
    private final Path path;

    public XmseqConfigurationParser(Path path) {
        requireNonNull(path);
        this.path = path;
    }

    public XmseqConfiguration parse(){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(Files.newInputStream(this.path));
            Element root = document.getDocumentElement();
            if(XmseqConfiguration.XML_TYPE_NAME.equals(root.getTagName())) {
                return XmseqConfiguration.parse(root);
            } else {
                String msg = "Configuration file is not of type XMSEQ !";
                logger.error(msg);
                throw new XmseqConfigurationLoadException(msg);
            }
        } catch(ParserConfigurationException e) {
            logger.error("Could not instantiate parser. Reason : " + e.getMessage());
            throw new XmseqConfigurationLoadException(e);
        } catch(IOException e) {
            logger.error("Could not properly read the configuration file. Reason : " + e.getMessage());
            throw new XmseqConfigurationLoadException(e);
        } catch(SAXException e) {
            logger.error("Configuration file seems to be not well-formed. Reason : " + e.getMessage());
            throw new XmseqConfigurationLoadException(e);
        }

    }
}
