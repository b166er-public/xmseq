package dm.audio.midi.xmseq.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmseqConfigurationPattern {
    private static final Logger logger = LogManager.getLogger(XmseqConfigurationPattern.class);
    public static final String XML_TYPE_NAME = "pattern";

    public static final String XML_TYPE_ATTACK_NAME = "attack";
    public static final String XML_TYPE_SUSTAIN_NAME = "sustain";
    public static final String XML_TYPE_RELEASE_NAME = "release";

    public final static String XML_ATTRIBUTE_ID = "id";
    public final static String XML_ATTRIBUTE_REF = "ref";
    public final static String XML_ATTRIBUTE_IMPORT = "import";

    private final XmseqConfigurationBusEventSequence attack;
    private final XmseqConfigurationBusEventSequence sustain;
    private final XmseqConfigurationBusEventSequence release;

    public XmseqConfigurationPattern(
        XmseqConfigurationBusEventSequence attack,
        XmseqConfigurationBusEventSequence sustain,
        XmseqConfigurationBusEventSequence release
    )
    {
        this.attack = attack;
        this.sustain = sustain;
        this.release = release;
    }

    public XmseqConfigurationBusEventSequence getAttack() {
        return attack;
    }

    public XmseqConfigurationBusEventSequence getSustain() {
        return sustain;
    }

    public XmseqConfigurationBusEventSequence getRelease() {
        return release;
    }

    public boolean hasAttack() {
        return (this.attack != null);
    }

    public boolean hasSustain() {
        return (this.sustain != null);
    }

    public boolean hasRelease() {
        return (this.release != null);
    }

    public static XmseqConfigurationPattern parse(
        XmseqConfiguration context,
        Element root
    )
    {
        boolean hasId = root.hasAttribute(XML_ATTRIBUTE_ID);
        boolean isRef = root.hasAttribute(XML_ATTRIBUTE_REF);
        boolean isImport = root.hasAttribute(XML_ATTRIBUTE_IMPORT);

        XmseqConfigurationPattern out = null;

        if(isRef) {
            String refValue = root.getAttribute(XML_ATTRIBUTE_REF);
            if(context.isPatternKnown(refValue)) {
                out = context.getPattern(refValue);
            } else {
                logger.error("The event sequence [ " + refValue + " ] is unknown!");
            }
        } else if(isImport) {
            logger.warn("Import of patterns is not implemented yet!");
        } else {
            XmseqConfigurationBusEventSequence attack = null;
            XmseqConfigurationBusEventSequence sustain = null;
            XmseqConfigurationBusEventSequence release = null;

            NodeList childNodes = root.getChildNodes();
            int numOfChilds = childNodes.getLength();
            for(int c = 0; c < numOfChilds; c++) {
                Node child = childNodes.item(c);
                boolean isElementNode = child.getNodeType() == Node.ELEMENT_NODE;
                if(isElementNode) {
                    Element elementChild = (Element) child;
                    String tagName = elementChild.getTagName();
                    if(XML_TYPE_ATTACK_NAME.equals(tagName)) {
                        attack = XmseqConfigurationBusEventSequence.parse(
                            context,
                            elementChild
                        );
                    } else if(XML_TYPE_SUSTAIN_NAME.equals(tagName)) {
                        sustain = XmseqConfigurationBusEventSequence.parse(
                            context,
                            elementChild
                        );
                    } else if(XML_TYPE_RELEASE_NAME.equals(tagName)) {
                        release = XmseqConfigurationBusEventSequence.parse(
                            context,
                            elementChild
                        );
                    } else {
                        logger.error("The pattern do not support a [" + tagName + "] section!");
                    }
                }
            }

            out = new XmseqConfigurationPattern(
                attack,
                sustain,
                release
            );
        }

        if(hasId) {
            context.putPattern(
                root.getAttribute(XML_ATTRIBUTE_ID),
                out
            );
        }

        return out;
    }
}
