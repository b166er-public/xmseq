package dm.audio.midi.xmseq.configuration;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmseqConfiguration {

    private static final Logger logger = LogManager.getLogger(XmseqConfiguration.class);

    public final static String XML_TYPE_NAME = "xmseq";

    public final static String XML_ATTRIBUTE_BPM = "bpm";
    public final static String XML_ATTRIBUTE_TPB = "tpb";

    private final Long bpm; // beat per minute
    private final Long tpb; // ticks per beat
    private final Map<String, XmseqConfigurationBusEventSequence> busEventSequenceMap = new HashMap<>();
    private final Map<String, XmseqConfigurationPattern> patternMap = new HashMap<>();
    private final List<XmseqConfigurationKeyBinding> keyBindingList = new ArrayList<>();

    public XmseqConfiguration(Long bpm, Long tpb) {
        this.bpm = bpm;
        this.tpb = tpb;
    }

    /**
     * SETTER
     * */
    public void putEventSequence(String name, XmseqConfigurationBusEventSequence eventSequence) {
        this.busEventSequenceMap.put(name,eventSequence);
    }

    public void putPattern(String name, XmseqConfigurationPattern pattern) {
        this.patternMap.put(name, pattern);
    }

    public void addKeyBinding(XmseqConfigurationKeyBinding keyBinding) {
        this.keyBindingList.add(keyBinding);
    }

    /**
     * GETTER
     * */
    public XmseqConfigurationBusEventSequence getEventSequence(String name) {
        return this.busEventSequenceMap.get(name);
    }

    public XmseqConfigurationPattern getPattern(String name) {
        return this.patternMap.get(name);
    }

    public List<XmseqConfigurationKeyBinding> getKeyBindingList() {
        return new ArrayList<>(this.keyBindingList);
    }

    public Long getBpm() {
        return bpm;
    }

    public Long getTpb() {
        return tpb;
    }

    /**
     * CHECKS
     * */
    public boolean isEventSequenceKnown(String name) {
        return this.busEventSequenceMap.containsKey(name);
    }

    public boolean isPatternKnown(String name) {
        return this.patternMap.containsKey(name);
    }

    public static XmseqConfiguration parse(Element root) {
        String attrBpm = root.getAttribute(XML_ATTRIBUTE_BPM);
        String attrTpb = root.getAttribute(XML_ATTRIBUTE_TPB);

        boolean isBpmSet = attrBpm.length() > 0;
        boolean isTpbSet = attrTpb.length() > 0;

        XmseqConfiguration out = null;

        if(isBpmSet && isTpbSet) {
            boolean isBpmValid = StringUtils.isNumeric(attrBpm);
            boolean isTpbValid = StringUtils.isNumeric(attrTpb);
            if(isBpmValid && isTpbValid) {
                long bpm = Long.parseLong(attrBpm);
                long tpb = Long.parseLong(attrTpb);
                out = new XmseqConfiguration(bpm,tpb);

                // Go through children
                NodeList childNodes = root.getChildNodes();
                int numOfChildren = childNodes.getLength();
                for(int c = 0; c < numOfChildren; c++) {
                    Node child = childNodes.item(c);
                    boolean isElementNode = child.getNodeType() == Node.ELEMENT_NODE;
                    if(isElementNode) {
                        Element childElement = (Element) child;
                        String tagName = childElement.getTagName();
                        if(XmseqConfigurationBusEventSequence.XML_TYPE_NAME.equals(tagName)) {
                            XmseqConfigurationBusEventSequence.parse(
                                out,
                                childElement
                            );
                        } else if(XmseqConfigurationPattern.XML_TYPE_NAME.equals(tagName)) {
                            XmseqConfigurationPattern.parse(
                                out,
                                childElement
                            );
                        } else if(XmseqConfigurationKeyBinding.XML_TYPE_NAME.equals(tagName)) {
                            out.addKeyBinding(
                                XmseqConfigurationKeyBinding.parse(
                                    out,
                                    childElement
                                )
                            );
                        } else {
                            logger.warn("An element of type [ " + tagName + " ] is not supported on top level !");
                        }
                    }
                }

            } else {
                logger.error("BPM and TPB has to be integers!");
            }
        } else {
            logger.error("A valid configuration needs the definition of BPM and TPB!");
        }

        return out;
    }
}
