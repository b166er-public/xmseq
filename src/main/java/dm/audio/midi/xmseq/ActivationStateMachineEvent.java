package dm.audio.midi.xmseq;

public enum ActivationStateMachineEvent {
    TRIGGER_PATTERN,
    STOP_PATTERN
}
