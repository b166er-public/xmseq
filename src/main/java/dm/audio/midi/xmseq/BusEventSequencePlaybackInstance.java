package dm.audio.midi.xmseq;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class BusEventSequencePlaybackInstance {

    public enum State {
        PLAYING,
        STOPPED
    }
    private final BusEventSequence sequence;

    private final long startTimeStamp;

    private long timeCursor = 0L;

    private State state = State.PLAYING;

    public BusEventSequencePlaybackInstance(BusEventSequence sequence, long startTimeStamp) {
        requireNonNull(sequence);
        this.sequence = sequence;
        this.startTimeStamp = startTimeStamp;
        this.timeCursor = startTimeStamp;
    }

    public boolean isPlaying() {
        return this.state == State.PLAYING;
    }

    public List<BusEvent> play(long currentTimeStamp) {

        List<BusEvent> out;

        if(this.state == State.PLAYING) {
            out = new ArrayList<>(
                    sequence.getTimeRange(
                            timeCursor,
                            currentTimeStamp,
                            startTimeStamp
                    )
            );
        } else {
            out = new ArrayList<>();
        }

        this.timeCursor = currentTimeStamp;

        if(this.state == State.PLAYING) {
            if( (this.timeCursor - this.startTimeStamp) > this.sequence.getDuration()) {
                this.state = State.STOPPED;
            }
        }

        return out;
    }

    public long getDuration() {
        return this.sequence.getDuration();
    }

    public long getLastTimeStamp() {
        return this.startTimeStamp + this.getDuration();
    }

}
