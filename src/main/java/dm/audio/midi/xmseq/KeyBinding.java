package dm.audio.midi.xmseq;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class KeyBinding {
    private final Key key;

    private final KeyBindingMode mode;
    private final long quantizationTime;
    private final List<VelocityMapping> velocityMappings = new ArrayList<>();

    public KeyBinding(Key key, KeyBindingMode mode, long quantizationTime, Pattern pattern) {
        requireNonNull(key);
        requireNonNull(pattern);
        this.key = key;
        this.mode = mode;
        this.quantizationTime = quantizationTime;
        this.velocityMappings.add(
            new VelocityMapping(
                VelocityMapping.VELOCITY_MIN_VALUE,
                VelocityMapping.VELOCITY_MAX_VALUE,
                pattern
            )
        );
    }

    public Key getKey() {
        return key;
    }

    public KeyBinding(Key key, KeyBindingMode mode, long quantizationTime, List<VelocityMapping> velocityMappings) {
        requireNonNull(key);
        this.key = key;
        this.mode = mode;
        this.quantizationTime = quantizationTime;
        this.velocityMappings.addAll(velocityMappings);
    }

    public boolean isResponsible(Key key, short velocity) {
        boolean isAssignedKey = this.key.equals(key);
        boolean hasVelocityMapping = this.velocityMappings.stream().anyMatch(vm -> {
            return vm.isResponsible(velocity);
        });
        return isAssignedKey && hasVelocityMapping;
    }

    public Pattern getPattern(short velocity) {
        return this.velocityMappings.stream().filter( vm -> {
            return vm.isResponsible(velocity);
        }).map(VelocityMapping::getPattern).findFirst().orElse(Pattern.EMPTY_PATTERN);
    }

    public long getQuantizationTime() {
        return this.quantizationTime;
    }

    public KeyBindingMode getMode() {
        return this.mode;
    }
}
