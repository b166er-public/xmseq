package dm.audio.midi.xmseq.messages;

import dm.audio.midi.xmseq.Key;

public class NoteOff extends KeyMessageBase {

    public static final String XML_TYPE_NAME = "note-off";

    public static final String XML_PARAMETER_VELOCITY = "velocity";
    public final short midiVelocity;

    public NoteOff(short midiChannel, short midiKey, short midiVelocity) {
        super(new Key(midiChannel,midiKey));
        this.midiVelocity = midiVelocity;
    }

    public Key getKey() {
        return this.key;
    }
}
