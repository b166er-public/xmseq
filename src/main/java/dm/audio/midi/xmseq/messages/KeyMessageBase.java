package dm.audio.midi.xmseq.messages;

import dm.audio.midi.xmseq.Key;

public class KeyMessageBase extends MessageBase {

    public static final String XML_PARAMETER_CHANNEL = "channel";
    public static final String XML_PARAMETER_KEY = "key";
    public final Key key;

    public KeyMessageBase(Key key) {
        this.key = key;
    }
}
