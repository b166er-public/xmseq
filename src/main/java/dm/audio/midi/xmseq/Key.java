package dm.audio.midi.xmseq;

import java.util.Objects;

public class Key {
    public final short midiChannel;
    public final short midiKey;

    public Key(short midiChannel, short midiKey) {
        this.midiChannel = midiChannel;
        this.midiKey = midiKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return midiChannel == key.midiChannel && midiKey == key.midiKey;
    }

    @Override
    public int hashCode() {
        return Objects.hash(midiChannel, midiKey);
    }
}
