package dm.audio.midi.xmseq;

import dm.audio.midi.xmseq.messages.MessageBase;

public class BusEvent implements Comparable<BusEvent> {
    private final long timestamp; // unit = microseconds
    private final MessageBase message;

    public BusEvent(long timestamp, MessageBase message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public MessageBase getMessage() {
        return message;
    }

    public int compareTo(BusEvent o) {
        return Long.compare(this.timestamp, o.timestamp);
    }
}
