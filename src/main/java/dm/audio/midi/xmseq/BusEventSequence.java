package dm.audio.midi.xmseq;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class BusEventSequence {

    public static final BusEventSequence EMPTY_EVENT_LIST = new BusEventSequence(0);

    private final List<BusEvent> list = new ArrayList<BusEvent>();
    private long duration;

    public BusEventSequence(long duration) { this.duration = duration; }

    public BusEventSequence(long duration, Collection<BusEvent> events) {
        this(duration);
        list.addAll(events);
        Collections.sort(list);
    }

    public BusEventSequence add(BusEvent event) {
        requireNonNull(event);
        list.add(event);
        Collections.sort(list);
        recalculateDuration();
        return this;
    }

    public BusEventSequence add(Collection<BusEvent> events) {
        requireNonNull(events);
        list.addAll(events);
        Collections.sort(list);
        recalculateDuration();
        return this;
    }

    private void recalculateDuration() {
        long tsMax =
            this.list.stream().mapToLong(
                be -> be.getTimestamp()
            ).max().orElse(0);

        if(tsMax > this.duration) {
            this.duration = tsMax;
        }
    }

    public BusEvent get(int idx) {
        return this.list.get(idx);
    }

    public List<BusEvent> getTimeRange(final long fromInclusive, final long untilExclusive) {
        List<BusEvent> out = new ArrayList<>();
        this.list.stream().forEach(
            be -> {
                long ts = be.getTimestamp();
                if(ts >= fromInclusive && ts < untilExclusive) {
                    out.add(be);
                }
            }
        );
        return out;
    }

    public List<BusEvent> getTimeRange(long fromInclusive, long untilExclusive, long offset) {
        return this.getTimeRange(
            fromInclusive - offset,
            untilExclusive - offset
        );
    }

    public long getDuration() {
        return duration;
    }
}
