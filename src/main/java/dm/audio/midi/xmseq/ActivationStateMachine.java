package dm.audio.midi.xmseq;

import java.util.Optional;

public class ActivationStateMachine {
    private ActivationState state = ActivationState.INACTIVE;

    public Optional<ActivationStateMachineEvent> processGateEvent(GateStateMachineEvent event) {
        Optional<ActivationStateMachineEvent> out = Optional.empty();

        switch(state) {
            case ACTIVE -> {
                switch(event) {
                    case GATE_CLOSED -> {out = Optional.of(ActivationStateMachineEvent.STOP_PATTERN);}
                    case GATE_OPEND -> {}
                }
            }
            case INACTIVE -> {
                switch(event) {
                    case GATE_CLOSED -> {}
                    case GATE_OPEND -> {out = Optional.of(ActivationStateMachineEvent.TRIGGER_PATTERN);}
                }
            }
        }

        return out;
    }

    public void processPatternPlaybackState(PatternPlaybackInstanceState state) {

        if(state == PatternPlaybackInstanceState.STOPPED) {
            this.state = ActivationState.INACTIVE;
        } else {
            this.state = ActivationState.ACTIVE;
        }
    }

    public ActivationState getState() {
        return this.state;
    }
}
