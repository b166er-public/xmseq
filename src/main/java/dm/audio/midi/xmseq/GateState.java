package dm.audio.midi.xmseq;

public enum GateState {
    OPEN,
    CLOSED
}
