package dm.audio.midi.xmseq;

public enum ActivationState {
    ACTIVE,
    INACTIVE
}
