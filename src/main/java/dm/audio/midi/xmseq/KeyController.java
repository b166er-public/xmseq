package dm.audio.midi.xmseq;

import dm.audio.midi.xmseq.messages.NoteOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class KeyController {

    private final KeyBinding keyBinding;
    private final KeyStateMachine keyStateMachine;
    private final GateStateMachine gateStateMachine;
    private final ActivationStateMachine activationStateMachine;

    private PatternPlaybackInstance patternPlaybackInstance = null;

    public KeyController(KeyBinding keyBinding) {
        this.keyBinding = keyBinding;
        this.keyStateMachine = new KeyStateMachine(keyBinding.getKey());
        this.gateStateMachine = new GateStateMachine(keyBinding.getMode());
        this.activationStateMachine = new ActivationStateMachine();
    }

    public void processBusEvent(BusEvent event, long currentTimeStamp) {
        Optional<KeyStateMachineEvent> keyEvent = keyStateMachine.processBusEvent(event);
        if(keyEvent.isPresent()) {
            Optional<GateStateMachineEvent> gateEvent = gateStateMachine.processKeyEvent(keyEvent.get());
            if(gateEvent.isPresent()) {
                Optional<ActivationStateMachineEvent> activationEvent = activationStateMachine.processGateEvent(gateEvent.get());
                if(activationEvent.isPresent()) {
                    ActivationStateMachineEvent actEvent = activationEvent.get();
                    switch(actEvent) {
                        case TRIGGER_PATTERN -> {
                            if(patternPlaybackInstance == null || patternPlaybackInstance.isStopped()) {
                                short velocity = ((NoteOn) event.getMessage()).midiVelocity;
                                Pattern p = keyBinding.getPattern(velocity);
                                patternPlaybackInstance = new PatternPlaybackInstance(
                                    p,
                                    currentTimeStamp,
                                    keyBinding.getQuantizationTime()
                                );
                            }
                        }
                        case STOP_PATTERN -> {
                            if(patternPlaybackInstance != null) {
                                patternPlaybackInstance.requestStop();
                            }
                        }
                    }

                }
            }
        }
    }

    public void processPlaybackInstanceState() {
        if(patternPlaybackInstance != null) {
            activationStateMachine.processPatternPlaybackState(patternPlaybackInstance.getState());
        } else {
            // Nothing to update as playback instance is null
        }
    }

    public List<BusEvent> play(long currentTimeStamp) {
        List<BusEvent> out = new ArrayList<>();

        if( (patternPlaybackInstance != null) && (patternPlaybackInstance.getState() != PatternPlaybackInstanceState.STOPPED) ) {
            out.addAll(
                patternPlaybackInstance.play(currentTimeStamp)
            );
        } else if( (patternPlaybackInstance != null) && (patternPlaybackInstance.getState() == PatternPlaybackInstanceState.STOPPED) ) {
            patternPlaybackInstance = null;
        }

        return out;
    }
}
