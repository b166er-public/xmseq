package dm.audio.midi.xmseq.utils;

public class Time {
    public static long nowInUs() {
        return (long)(((double)System.nanoTime()) / 1000.0);
    }

    public static long usPerBeat(long bpm) {
        double usPerBeat = (1000000.0 * 60) / ((double)bpm);
        return ((long)usPerBeat);
    }
}
