package dm.audio.midi.xmseq;

public enum KeyBindingMode {
    HOLD,
    TOGGLE
}
