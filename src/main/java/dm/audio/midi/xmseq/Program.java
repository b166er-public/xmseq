package dm.audio.midi.xmseq;

import dm.audio.midi.xmseq.configuration.XmseqConfiguration;
import dm.audio.midi.xmseq.configuration.XmseqConfigurationParser;
import dm.audio.midi.xmseq.runtime.XmseqEngine;
import dm.audio.midi.xmseq.runtime.XmseqMidiDeviceSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sound.midi.MidiUnavailableException;
import java.nio.file.Path;

public class Program {

    private static final Logger logger = LogManager.getLogger(Program.class);
    private static final String ENV_CONFIG_PATH = "XMSEQ_CONFIG_FILE";

    private static XmseqConfiguration configuration = null;
    private static XmseqEngine engine = null;

    public static void main(String[] args) {
        showBanner();
        loadConfiguration();
        if(Program.configuration != null) {
            registerSystemTerminationRequestHandler();
            runEngine();
        } else {
            logger.error("No proper configuration file had been passed!");
        }
        freeResources();
        logger.info("Session closed. Goodbye!");
    }

    private static void showBanner() {

        StringBuilder sb = new StringBuilder();

        sb.append(" _  _  __  __  ___  ____  _____ \n");
        sb.append("( \\/ )(  \\/  )/ __)( ___)(  _  )\n");
        sb.append(" )  (  )    ( \\__ \\ )__)  )(_)( \n");
        sb.append("(_/\\_)(_/\\/\\_)(___/(____)(___/\\\\\n");
        sb.append("\n");

        try {
            sb.append("==========[ MIDI DEVICES ]==========");
            sb.append("\n");
            sb.append("\n");

            XmseqMidiDeviceSupport.getDeviceMap().entrySet().stream().forEach(
                entry -> {
                    sb.append(
                        "ID [%s] >>> %s\n".formatted(
                            entry.getKey(), entry.getValue()
                        )
                    );
                }
            );

            sb.append("\n");
            sb.append("====================================");
            sb.append("\n");
            sb.append("\n");
        } catch(Exception e) {
            logger.error("Could not get proper access to MIDI system! Reason: " + e.getMessage());
            System.exit(-1);
        }

        System.out.print(sb);
    }

    private static void freeResources() {
        // TODO
    }

    private static void runEngine() {
        Program.engine = new XmseqEngine(Program.configuration);
        Program.engine.init();
        if(Program.engine.isInitialized()) {
            Program.engine.run();
        } else {
            logger.error("Engine could not be initialized!");
        }
    }

    private static void registerSystemTerminationRequestHandler() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
            if(Program.engine != null) {
                Program.engine.requestTermination();
            }
            }
        });
    }

    private static void loadConfiguration() {
        String configFilePath = System.getenv(ENV_CONFIG_PATH);
        if(configFilePath == null) {
            logger.error("No configuration file had been provided! Please define " + ENV_CONFIG_PATH + " in your environment!");
            System.exit(-1);
        } else {
            try {
                XmseqConfigurationParser parser = new XmseqConfigurationParser(
                    Path.of(configFilePath)
                );
                Program.configuration = parser.parse();
            }
            catch(Exception e) {
                logger.error("Configuration file could not be parsed. Reason : " + e.getMessage());
                System.exit(-1);
            }
        }
    }

}
