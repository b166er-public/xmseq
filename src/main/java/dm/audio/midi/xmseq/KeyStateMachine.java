package dm.audio.midi.xmseq;

import dm.audio.midi.xmseq.messages.KeyMessageBase;
import dm.audio.midi.xmseq.messages.MessageBase;
import dm.audio.midi.xmseq.messages.NoteOff;
import dm.audio.midi.xmseq.messages.NoteOn;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class KeyStateMachine {
    private KeyState state = KeyState.RELEASED;
    private final Key responsibleKey;

    public KeyStateMachine(Key responsibleKey) {
        requireNonNull(responsibleKey);
        this.responsibleKey = responsibleKey;
    }

    /**
     * Returns true if key state had been changed
     * */
    public Optional<KeyStateMachineEvent> processBusEvent(BusEvent event) {
        MessageBase message = event.getMessage();

        if(message instanceof KeyMessageBase keyMessage) {
            if(isResponsible(keyMessage.key)) {
                if(message instanceof NoteOn) {
                    switch(state) {
                        case PRESSED -> { return Optional.empty(); }
                        case RELEASED -> {
                            state = KeyState.PRESSED;
                            return Optional.of(KeyStateMachineEvent.KEY_PRESSED);
                        }
                    }
                } else if(message instanceof NoteOff) {
                    switch(state) {
                        case PRESSED -> {
                            state = KeyState.RELEASED;
                            return Optional.of(KeyStateMachineEvent.KEY_RELEASED);
                        }
                        case RELEASED -> { return Optional.empty(); }
                    }
                } else {
                    // I do not care about other events
                }
            } else {
                // This state machine is not responsible for this key
            }
        } else {
            // Only handle key based messages
        }

        return Optional.empty();
    }

    private boolean isResponsible(Key key) {
        return this.responsibleKey.equals(key);
    }

    public KeyState getState() {
        return this.state;
    }
}
