package dm.audio.midi.xmseq;

import java.util.Objects;

import static dm.audio.midi.xmseq.BusEventSequence.EMPTY_EVENT_LIST;

public class Pattern {

    public static final Pattern EMPTY_PATTERN = new Pattern(
        EMPTY_EVENT_LIST,
        EMPTY_EVENT_LIST,
        EMPTY_EVENT_LIST
    );

    private final BusEventSequence attack;
    private final BusEventSequence sustain;
    private final BusEventSequence release;

    public Pattern(BusEventSequence attack, BusEventSequence sustain, BusEventSequence release) {

        this.attack = Objects.requireNonNullElse(attack, EMPTY_EVENT_LIST);
        this.sustain = Objects.requireNonNullElse(sustain, EMPTY_EVENT_LIST);
        this.release = Objects.requireNonNullElse(release, EMPTY_EVENT_LIST);
    }

    public BusEventSequence getAttack() {
        return attack;
    }

    public BusEventSequence getSustain() {
        return sustain;
    }

    public BusEventSequence getRelease() {
        return release;
    }
}
